﻿using Microsoft.EntityFrameworkCore;
using SIPAccountManager.Models.DataBase;

namespace SIPAccountManager.Data.DbContexts
{
    public class DataDbContext : DbContext
    {
        public DataDbContext (DbContextOptions<DataDbContext> options)
            : base(options)
        {
        }

        public DbSet<SipAccount> SipAccounts { get; set; }
        public DbSet<WindowsUser> WindowsUsers { get; set; }
        public DbSet<GSGXPPhone> GSGXPPhones { get; set; }
        public DbSet<WorkPlace> WorkPlaces { get; set; }
    }
}
