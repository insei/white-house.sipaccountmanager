﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SIPAccountManager.Data.DbContexts;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SIPAccountManager.Data
{
    public class SeedDataBase
    {
        public static void Initialize(IServiceProvider serviceProvider, IConfiguration configuration)
        {
            var context = serviceProvider.GetRequiredService<ApplicationDbContext>();
            var contextData = serviceProvider.GetRequiredService<DataDbContext>();
            var userManager = serviceProvider.GetRequiredService<UserManager<IdentityUser>>();
            context.Database.Migrate();
            contextData.Database.Migrate();
            if (!context.Users.Any())
            {
                var defaultUsersConf = configuration.GetSection("DefaultUsers");
                foreach (var defUserConf in defaultUsersConf.GetChildren())
                {
                    var username = defUserConf.GetValue<string>("username");
                    var password = defUserConf.GetValue<string>("password");
                    var email = defUserConf.GetValue<string>("email");

                    var user = new IdentityUser()
                    {
                        Email = email,
                        SecurityStamp = Guid.NewGuid().ToString(),
                        UserName = username,
                    };

                    Task.Run(() => userManager.CreateAsync(user, password)).Wait();
                }
            }
        }
    }
}
