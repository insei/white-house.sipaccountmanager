﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SIPAccountManager.Data.DbContexts;
using SIPAccountManager.Models.DataBase;

namespace SIPAccountManager.Controllers
{
    [Authorize]
    public class WorkPlacesController : Controller
    {
        private readonly DataDbContext _context;

        public WorkPlacesController(DataDbContext context)
        {
            _context = context;
        }

        private List<GSGXPPhone> GetAvailableGSGXPPhonesToSet(int? currentGSGXPPhoneId = null)
        {
            List<GSGXPPhone> unsettedGSGXPPhones;
            var settedGSGXPPhoneIds = _context.WorkPlaces.Select(s => s.GSGXPPhoneId).Where(Id => Id != null).ToArray();
            if (currentGSGXPPhoneId != null)
                unsettedGSGXPPhones = _context.GSGXPPhones.Where(gsgxpp => !settedGSGXPPhoneIds.Contains(gsgxpp.Id) || gsgxpp.Id == currentGSGXPPhoneId).ToList();
            else
            {
                unsettedGSGXPPhones = _context.GSGXPPhones.Where(gsgxpp => !settedGSGXPPhoneIds.Contains(gsgxpp.Id)).ToList();
            }
            unsettedGSGXPPhones.Add(new GSGXPPhone() { Id = 0 });
            return unsettedGSGXPPhones;
        }

        // GET: WorkPlaces
        public async Task<IActionResult> Index()
        {
            var dataDbContext = _context.WorkPlaces.Include(w => w.GSGXPPhone);
            return View(await dataDbContext.ToListAsync());
        }

        // GET: WorkPlaces/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var workPlace = await _context.WorkPlaces
                .Include(w => w.GSGXPPhone)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (workPlace == null)
            {
                return NotFound();
            }

            return View(workPlace);
        }

        // GET: WorkPlaces/Create
        public IActionResult Create()
        {
            ViewData["GSGXPPhoneId"] = new SelectList(GetAvailableGSGXPPhonesToSet(), "Id", "Name", 0);
            return View();
        }

        // POST: WorkPlaces/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,PCName,GSGXPPhoneId")] WorkPlace workPlace)
        {
            if (ModelState.IsValid)
            {
                if (workPlace.GSGXPPhoneId == 0)
                    workPlace.GSGXPPhoneId = null;
                _context.Add(workPlace);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["GSGXPPhoneId"] = new SelectList(GetAvailableGSGXPPhonesToSet(workPlace.GSGXPPhoneId), "Id", "Name", workPlace.GSGXPPhoneId);
            return View(workPlace);
        }

        // GET: WorkPlaces/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var workPlace = await _context.WorkPlaces.FindAsync(id);
            if (workPlace == null)
            {
                return NotFound();
            }
            ViewData["GSGXPPhoneId"] = new SelectList(GetAvailableGSGXPPhonesToSet(workPlace.GSGXPPhoneId), "Id", "Name", workPlace.GSGXPPhoneId);
            // set to show empty selectedlist item
            if (workPlace.GSGXPPhoneId == null)
                workPlace.GSGXPPhoneId = 0;
            return View(workPlace);
        }

        // POST: WorkPlaces/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,PCName,GSGXPPhoneId")] WorkPlace workPlace)
        {
            if (id != workPlace.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (workPlace.GSGXPPhoneId == 0)
                        workPlace.GSGXPPhoneId = null;
                    _context.Update(workPlace);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!WorkPlaceExists(workPlace.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["GSGXPPhoneId"] = new SelectList(GetAvailableGSGXPPhonesToSet(workPlace.GSGXPPhoneId), "Id", "Name", workPlace.GSGXPPhoneId);
            return View(workPlace);
        }

        // GET: WorkPlaces/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var workPlace = await _context.WorkPlaces
                .Include(w => w.GSGXPPhone)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (workPlace == null)
            {
                return NotFound();
            }

            return View(workPlace);
        }

        // POST: WorkPlaces/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var workPlace = await _context.WorkPlaces.FindAsync(id);
            _context.WorkPlaces.Remove(workPlace);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool WorkPlaceExists(int id)
        {
            return _context.WorkPlaces.Any(e => e.Id == id);
        }
    }
}
