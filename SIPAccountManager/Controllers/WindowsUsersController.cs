﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SIPAccountManager.Data.DbContexts;
using SIPAccountManager.Models.DataBase;

namespace SIPAccountManager.Controllers
{
    [Authorize]
    public class WindowsUsersController : Controller
    {
        private readonly DataDbContext _context;

        public WindowsUsersController(DataDbContext context)
        {
            _context = context;
        }

        private List<SipAccount> GetAvailableSipAccountsToSet(int? currentSipAccountId = null)
        {
            List<SipAccount> unsettedAccounts;
            var settedAccountIds = _context.WindowsUsers.Select(s => s.SipAccountId).Where(Id => Id != null).ToArray();
            if(currentSipAccountId != null)
                unsettedAccounts = _context.SipAccounts.Where(acc => !settedAccountIds.Contains(acc.Id) || acc.Id == currentSipAccountId).ToList();
            else
            {
                unsettedAccounts = _context.SipAccounts.Where(acc => !settedAccountIds.Contains(acc.Id)).ToList();
            }
            unsettedAccounts.Add(new SipAccount() { Id = 0 });
            return unsettedAccounts;
        }

        // GET: WindowsUsers
        public async Task<IActionResult> Index()
        {
            var dataDbContext = _context.WindowsUsers.Include(w => w.SipAccount);
            return View(await dataDbContext.ToListAsync());
        }

        // GET: WindowsUsers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var windowsUser = await _context.WindowsUsers
                .Include(w => w.SipAccount)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (windowsUser == null)
            {
                return NotFound();
            }

            return View(windowsUser);
        }

        // GET: WindowsUsers/Create
        public IActionResult Create()
        {
            ViewData["SipAccountId"] = new SelectList(GetAvailableSipAccountsToSet(), "Id", "Exten", 0);
            return View();
        }

        // POST: WindowsUsers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,SID,Username,SipAccountId")] WindowsUser windowsUser)
        {
            if (ModelState.IsValid)
            {
                if (windowsUser.SipAccountId == 0)
                    windowsUser.SipAccountId = null;
                _context.Add(windowsUser);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["SipAccountId"] = new SelectList(GetAvailableSipAccountsToSet(windowsUser.SipAccountId), "Id", "Exten", windowsUser.SipAccountId);
            return View(windowsUser);
        }

        // GET: WindowsUsers/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var windowsUser = await _context.WindowsUsers.FindAsync(id);
            if (windowsUser == null)
            {
                return NotFound();
            }
            ViewData["SipAccountId"] = new SelectList(GetAvailableSipAccountsToSet(windowsUser.SipAccountId), "Id", "Exten", windowsUser.SipAccountId);
            // set to show empty selectedlist item
            if (windowsUser.SipAccountId == null)
                windowsUser.SipAccountId = 0;
            return View(windowsUser);
        }

        // POST: WindowsUsers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,SID,Username,SipAccountId")] WindowsUser windowsUser)
        {
            if (id != windowsUser.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (windowsUser.SipAccountId == 0)
                        windowsUser.SipAccountId = null;
                    _context.Update(windowsUser);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!WindowsUserExists(windowsUser.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["SipAccountId"] = new SelectList(GetAvailableSipAccountsToSet(windowsUser.SipAccountId), "Id", "Exten", windowsUser.SipAccountId);
            return View(windowsUser);
        }

        // GET: WindowsUsers/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var windowsUser = await _context.WindowsUsers
                .Include(w => w.SipAccount)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (windowsUser == null)
            {
                return NotFound();
            }

            return View(windowsUser);
        }

        // POST: WindowsUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var windowsUser = await _context.WindowsUsers.FindAsync(id);
            _context.WindowsUsers.Remove(windowsUser);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool WindowsUserExists(int id)
        {
            return _context.WindowsUsers.Any(e => e.Id == id);
        }
    }
}
