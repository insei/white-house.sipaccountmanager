﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SIPAccountManager.Data.DbContexts;
using SIPAccountManager.Models.DataBase;

namespace SIPAccountManager.Controllers
{
    [Authorize]
    public class GSGXPPhonesController : Controller
    {
        private readonly DataDbContext _context;

        public GSGXPPhonesController(DataDbContext context)
        {
            _context = context;
        }

        // GET: GSGXPPhones
        public async Task<IActionResult> Index()
        {
            var dataDbContext = _context.GSGXPPhones.Include(g => g.SipAccount);
            return View(await dataDbContext.ToListAsync());
        }

        // GET: GSGXPPhones/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var gSGXPPhone = await _context.GSGXPPhones
                .Include(g => g.SipAccount)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (gSGXPPhone == null)
            {
                return NotFound();
            }

            return View(gSGXPPhone);
        }

        // GET: GSGXPPhones/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: GSGXPPhones/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Address,Username,Password,Model,SipAccountId")] GSGXPPhone gSGXPPhone)
        {
            if (ModelState.IsValid)
            {
                _context.Add(gSGXPPhone);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(gSGXPPhone);
        }

        // GET: GSGXPPhones/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var gSGXPPhone = await _context.GSGXPPhones.Include(g => g.SipAccount)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (gSGXPPhone == null)
            {
                return NotFound();
            }

            SipAccount[] sipAccounts = new SipAccount[] { };
            if (gSGXPPhone.SipAccount != null)
                sipAccounts = new[] { gSGXPPhone.SipAccount };

            ViewData["SipAccountId"] = new SelectList(sipAccounts, "Id", "Exten", gSGXPPhone.SipAccountId);
            return View(gSGXPPhone);
        }

        // POST: GSGXPPhones/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Address,Username,Password,Model,SipAccountId")] GSGXPPhone gSGXPPhone)
        {
            if (id != gSGXPPhone.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(gSGXPPhone);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!GSGXPPhoneExists(gSGXPPhone.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(gSGXPPhone);
        }

        // GET: GSGXPPhones/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var gSGXPPhone = await _context.GSGXPPhones
                .Include(g => g.SipAccount)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (gSGXPPhone == null)
            {
                return NotFound();
            }
            if (_context.WorkPlaces.Any(u => u.GSGXPPhoneId == id))
                ViewData["Error"] = "You cannot delete this element because it is used in other elements.";
            return View(gSGXPPhone);
        }

        // POST: GSGXPPhones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (!_context.WorkPlaces.Any(u => u.GSGXPPhoneId == id))
            {
                var gSGXPPhone = await _context.GSGXPPhones.FindAsync(id);
                _context.GSGXPPhones.Remove(gSGXPPhone);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return RedirectToAction(nameof(Delete), id);
        }

        private bool GSGXPPhoneExists(int id)
        {
            return _context.GSGXPPhones.Any(e => e.Id == id);
        }
    }
}
