﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SIPAccountManager.Data.DbContexts;
using SIPAccountManager.Models.DataBase;

namespace SIPAccountManager.Controllers
{
    [Authorize]
    public class SipAccountsController : Controller
    {
        private readonly DataDbContext _context;

        public SipAccountsController(DataDbContext context)
        {
            _context = context;
        }

        // GET: SipAccounts
        public async Task<IActionResult> Index()
        {
            return View(await _context.SipAccounts.ToListAsync());
        }

        // GET: SipAccounts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sipAccount = await _context.SipAccounts
                .FirstOrDefaultAsync(m => m.Id == id);
            if (sipAccount == null)
            {
                return NotFound();
            }

            return View(sipAccount);
        }

        // GET: SipAccounts/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: SipAccounts/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Exten,Server,Username,Password")] SipAccount sipAccount)
        {
            if (ModelState.IsValid)
            {
                _context.Add(sipAccount);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(sipAccount);
        }

        // GET: SipAccounts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sipAccount = await _context.SipAccounts.FindAsync(id);
            if (sipAccount == null)
            {
                return NotFound();
            }
            return View(sipAccount);
        }

        // POST: SipAccounts/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Exten,Server,Username,Password")] SipAccount sipAccount)
        {
            if (id != sipAccount.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(sipAccount);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SipAccountExists(sipAccount.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(sipAccount);
        }

        // GET: SipAccounts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sipAccount = await _context.SipAccounts
                .FirstOrDefaultAsync(m => m.Id == id);
            if (sipAccount == null)
            {
                return NotFound();
            }
            if (_context.WindowsUsers.Any(u => u.SipAccountId == id)
                || _context.GSGXPPhones.Any(p => p.SipAccountId == id))
                ViewData["Error"] = "You cannot delete this element because it is used in other elements.";
            return View(sipAccount);
        }

        // POST: SipAccounts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (!_context.WindowsUsers.Any(u => u.SipAccountId == id))
            {
                var sipAccount = await _context.SipAccounts.FindAsync(id);
                _context.SipAccounts.Remove(sipAccount);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index), id);
            }
            return RedirectToAction(nameof(Delete), id);
        }

        private bool SipAccountExists(int id)
        {
            return _context.SipAccounts.Any(e => e.Id == id);
        }
    }
}
