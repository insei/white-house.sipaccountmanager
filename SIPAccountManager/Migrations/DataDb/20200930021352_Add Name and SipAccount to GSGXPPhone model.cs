﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SIPAccountManager.Migrations.DataDb
{
    public partial class AddNameandSipAccounttoGSGXPPhonemodel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "GSGXPPhones",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SipAccountId",
                table: "GSGXPPhones",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_GSGXPPhones_SipAccountId",
                table: "GSGXPPhones",
                column: "SipAccountId");

            migrationBuilder.AddForeignKey(
                name: "FK_GSGXPPhones_SipAccounts_SipAccountId",
                table: "GSGXPPhones",
                column: "SipAccountId",
                principalTable: "SipAccounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GSGXPPhones_SipAccounts_SipAccountId",
                table: "GSGXPPhones");

            migrationBuilder.DropIndex(
                name: "IX_GSGXPPhones_SipAccountId",
                table: "GSGXPPhones");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "GSGXPPhones");

            migrationBuilder.DropColumn(
                name: "SipAccountId",
                table: "GSGXPPhones");
        }
    }
}
