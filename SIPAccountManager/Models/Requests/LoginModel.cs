﻿using System.ComponentModel.DataAnnotations;

namespace SIPAccountManager.Models.Requests
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Это обазательное поле")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Это обазательное поле")]
        public string Password { get; set; }
    }
}
