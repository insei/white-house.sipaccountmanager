﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIPAccountManager.Models.DataBase
{
    public class SipAccount
    {
        public int Id { get; set; }
        public string Exten { get; set; }
        public string Server { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
