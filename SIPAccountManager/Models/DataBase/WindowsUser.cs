﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SIPAccountManager.Models.DataBase
{
    public class WindowsUser
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Это обязательное поле")]
        public string SID { get; set; }
        public string Username { get; set; }
        [Display(Name = "Sip Account")]
        public int? SipAccountId { get; set; }
        [Display(Name = "Sip Account")]
        public virtual SipAccount SipAccount { get; set; }
    }
}
