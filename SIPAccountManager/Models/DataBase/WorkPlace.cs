﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SIPAccountManager.Models.DataBase
{
    public class WorkPlace
    {
        public int Id { get; set; }
        [Display(Name = "PC Name")]
        public string PCName { get; set; }
        [Display(Name = "GS GXP Phone")]
        public int? GSGXPPhoneId { get; set; }
        [Display(Name = "GS GXP Phone")]
        public GSGXPPhone GSGXPPhone { get; set; }
    }
}
