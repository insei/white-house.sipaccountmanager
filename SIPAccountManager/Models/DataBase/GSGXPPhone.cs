﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SIPAccountManager.Models.DataBase
{
    public class GSGXPPhone
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int Model { get; set; }
        [Display(Name = "Current Sip Account")]
        public int? SipAccountId { get; set; }
        [Display(Name = "Current Sip Account")]
        public SipAccount SipAccount { get; set; }
    }
}
